// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
     production: false,
     firebase :{
          apiKey: "AIzaSyBubjxaGNXEyctCk_VVat9ujNtLlePTEAM",
          authDomain: "sef-ffg.firebaseapp.com",
          projectId: "sef-ffg",
          storageBucket: "sef-ffg.appspot.com",
          messagingSenderId: "791945068622",
          appId: "1:791945068622:web:099a35ca41127dd4f0831f",
          measurementId: "G-0S23GK02F3"
        }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
