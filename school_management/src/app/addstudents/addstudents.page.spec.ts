import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddstudentsPage } from './addstudents.page';

describe('AddstudentsPage', () => {
  let component: AddstudentsPage;
  let fixture: ComponentFixture<AddstudentsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddstudentsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddstudentsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
