import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddstudentsPage } from './addstudents.page';

const routes: Routes = [
  {
    path: '',
    component: AddstudentsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddstudentsPageRoutingModule {}
