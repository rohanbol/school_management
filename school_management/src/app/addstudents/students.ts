export class StudentRecord {
    rollNo: string;
    name: string;
    guardianName: string;
    aadharId: string;
    dateofBirth: string;
    gender: string;
    classId: string;
    schoolId: string;
}