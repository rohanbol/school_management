import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Student } from '../shared/student';
import { CurdService } from '../shared/curd.service';
import { ActivatedRoute } from '@angular/router';
import { StudentRecord } from './students';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProfileModel } from '../profile/profile.model';

import {IonContent} from '@ionic/angular';

@Component({
  selector: 'app-addstudents',
  templateUrl: './addstudents.page.html',
  styleUrls: ['./addstudents.page.scss'],
})
export class AddstudentsPage implements OnInit {

  StudentDetailsForm: FormGroup;

  userProfile: ProfileModel;

  editStudentName: string;
  editRollNo: string;
  editGender: string;
  editGuardianName: string;
  editAadharId: string;
  editDateofBirth: string;
  schoolId: string = null;

  classId: string = null;

  allClassesForASchool = [];

  public records: any[] = [];

  addStudents: boolean;

  showSpinner: boolean;

  students: Student[];

  schools: any = [];

  param: boolean = false;

  isTeacher: boolean = false;

  @ViewChild('studentsForm',{static: true}) content: IonContent  ;

  constructor(private firestore: AngularFirestore, private curdService: CurdService, private route: ActivatedRoute) {
    
    route.params.subscribe(params => {
      if (params.schoolId && params.className) {
        this.param = true;
        this.schoolId = params.schoolId;
        this.classId = params.className;
        this.refreshData()
      } else {
        this.userProfile = JSON.parse(localStorage.getItem('current_user'));
        this.param = false;
        this.schoolsList()
        if(this.userProfile?.role.includes('teacher')){
          this.isTeacher=true;
          this.schoolId = this.userProfile.school;
        }
        

      }


    });

  }

  ngOnInit() {

    this.userProfile = JSON.parse(localStorage.getItem('current_user'));
    this.StudentDetailsForm = new FormGroup({
      name: new FormControl('', Validators.compose([
        Validators.required, Validators.pattern('^[a-zA-Z]+(([ ][a-zA-Z ])?[a-zA-Z]*)*$')])),
      rollNo: new FormControl('', Validators.compose([
        Validators.required, Validators.pattern('[0-9a-zA-Z]+')])),
      gender: new FormControl('', Validators.required),
      guardianName: new FormControl('', Validators.compose([
        Validators.required, Validators.pattern('^[a-zA-Z]+(([ ][a-zA-Z ])?[a-zA-Z]*)*$')])),
      aadharId: new FormControl('', Validators.required),
      dateofBirth: new FormControl('', Validators.required)
    });

    if (this.schoolId) {
      this.classList(this.schoolId);
    }
    if (this.classId) {
      this.refreshData()
    }

  }

  getDataClass($event: any) {
    this.refreshData();
  }

  getDataSchool($event: any) {
    this.classList(this.schoolId);
  }

  schoolsList() {
    const schools = this.firestore.collection('Schools').get().subscribe((doc) => {
      doc.forEach(e => {
        this.schools.push(e.id.toString());
      });

    });

  }

  classList(schoolId: string) {
    this.firestore.collection('Schools/' + schoolId + '/Classes').get().subscribe(res => {
      this.allClassesForASchool = res.docs.map(doc => {
        return { id: doc.id, ...Object.assign({}, doc.data()) };
      });

    });
  }


  refreshData() {
    this.students = [];


    if (this.classId && this.schoolId) {
      this.curdService.getStudentsFilter(this.classId, this.schoolId).subscribe(stu => {
        this.students = stu;
      });
    }

  }

  logForm() {

    const path = 'Schools/' + this.schoolId + '/Classes/' + this.classId + '/Students';

    const studentCollection = this.firestore.collection(path);

    let dateString = this.StudentDetailsForm.value['dateofBirth'];

    let newDate = new Date(dateString);

    const newStudent = {
      rollNo: this.StudentDetailsForm.value['rollNo'],
      classId: this.classId,
      schoolId: this.schoolId,
      guardianName: this.StudentDetailsForm.value['guardianName'],
      name: this.StudentDetailsForm.value['name'],
      aadharId: this.StudentDetailsForm.value['aadharId'],
      dateofBirth: newDate.toLocaleDateString(),
      gender: this.StudentDetailsForm.value['gender']
    };

    studentCollection.doc(this.StudentDetailsForm.value['rollNo']).set(newStudent);

    this.StudentDetailsForm.reset();
  }

  updateStudentRecord(updateStudentName: string, updateStudentRollNo: string, updateStudentGender: string, updateStudentGuardianName: string,
    
    
    updateStudentAadharId: string, updateStudnetDateOfBirth: string) {

    this.editRollNo = updateStudentRollNo;
    this.editStudentName = updateStudentName;
    this.editAadharId = updateStudentAadharId;
    //this.editDateofBirth = this.datePipe.transform(updateStudnetDateOfBirth, 'yyyy-MM-dd');
    // this.editGender = updateStudentGender;
    this.editGuardianName = updateStudentGuardianName;

    let yOffset = document.getElementById('studentForm').offsetTop;
    console.log(yOffset);
    this.content.scrollToPoint(0,yOffset, 0);
  }

  


  deleteStudentRecord(deleteRollNo: string) {

    const path = 'Schools/' + this.schoolId + '/Classes/' + this.classId + '/Students';

    const studentCollection = this.firestore.collection(path);

    studentCollection.doc(deleteRollNo).delete();

  }

  onSubmit() {
    this.addStudents = false;
    this.showSpinner = true;
    for (var rec of this.records) {
      const path = 'Schools/' + this.schoolId + '/Classes/' + this.classId + '/Students';

      const studentCollection = this.firestore.collection(path);

      const newStudent = {
        rollNo: rec.rollNo.valueOf(),
        classId: this.classId.valueOf(),
        schoolId: this.schoolId.valueOf(),
        guardianName: rec.guardianName.valueOf(),
        aadharId: rec.aadharId.valueOf(),
        dateofBirth: rec.dateofBirth.valueOf(),
        gender: rec.gender.valueOf(),
        name: rec.name
      };

      studentCollection.doc(rec.rollNo).set(newStudent);

    }
    this.showSpinner = false;
    this.fileReset();
  }

  uploadListener($event: any): void {
    let text = [];
    let files = $event.srcElement.files;
    this.addStudents = true;
    if (this.isValidCSVFile(files[0])) {
      let input = $event.target;
      let reader = new FileReader();
      reader.readAsText(input.files[0]);

      reader.onload = () => {
        let csvData = reader.result;
        let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);

        let headersRow = this.getHeaderArray(csvRecordsArray);

        this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);

      };

      reader.onerror = function () {
        console.log('error is occured while reading file!');
      };

    } else {
      alert("Please import valid .csv file.");
      this.fileReset();
    }
  }
  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
    let csvArr = [];

    for (let i = 1; i < csvRecordsArray.length; i++) {
      let curruntRecord = (<string>csvRecordsArray[i]).split(',');
      if (curruntRecord.length == headerLength) {
        let csvRecord: StudentRecord = new StudentRecord();
        csvRecord.rollNo = curruntRecord[1].trim();
        csvRecord.name = curruntRecord[0].trim();
        csvRecord.guardianName = curruntRecord[2].trim();
        csvRecord.classId = this.classId;
        csvRecord.schoolId = this.schoolId;
        csvRecord.aadharId = curruntRecord[3].trim();
        csvRecord.dateofBirth = curruntRecord[4].trim();
        csvRecord.gender = curruntRecord[5].trim();
        csvArr.push(csvRecord);
      }
    }
    return csvArr;
  }

  isValidCSVFile(file: any) {
    return file.name.endsWith(".csv");
  }

  getHeaderArray(csvRecordsArr: any) {
    let headers = (<string>csvRecordsArr[0]).split(',');
    let headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  fileReset() {
    this.records = [];
  }
}
