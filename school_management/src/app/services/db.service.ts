import { ModifyPage } from './../assessments/modify/modify.page';
import { ProfileModel } from './../profile/profile.model';
import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage'
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Student } from '../shared/student';
import { AlertController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class DbService {

  profileCollectionEmail: AngularFirestoreCollection<ProfileModel>;
  profileCollection: AngularFirestoreCollection<ProfileModel>;
  userProfile: AngularFirestoreCollection<ProfileModel>;
  profile: any;
  studentsCollectionFilter: AngularFirestoreCollection<Student>;
  students: Observable<Student[]>;
  studentsFilter: Observable<Student[]>;
  studentDoc: AngularFirestoreDocument<Student>;
  questionCollection: AngularFirestoreCollection<any>;
  assessmentsCollection: AngularFirestoreCollection<any>;
  assessments: any;
  questions: any;


  constructor( private afs: AngularFirestore,public alertController: AlertController) {
    console.log(afs)

  }

  public getProfile(number) {
    this.profileCollection = this.afs.collection('Users', ref => ref.where("phoneNumber", "==", number));
    console.log("Inside DB Service with Number: " + number);
    return this.profileCollection.doc(number).get();
  }

  public getProfileByEmail(email) {
    this.profileCollectionEmail = this.afs.collection('Users', ref => ref.where("email", "==", email));
    this.profile = this.profileCollectionEmail.snapshotChanges().pipe(map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as ProfileModel;
        data.id = a.payload.doc.id;
        return data;
      });
    }));
    console.log(this.profile);
    return this.profile;
  }


  addUser(user) {
    console.log(user);
    this.profileCollection = this.afs.collection('Users', ref => ref.where("phoneNumber", "==", user.phoneNumber));
    this.profileCollection.doc(user.phoneNumber).set(Object.assign({}, user));
    this.profileCollection = this.afs.collection('Users');
    return this.profileCollection.get();
  }


  updateUser(updatedUser, oldUser) {
    if (updatedUser.phoneNumber != oldUser.phoneNumber) {
      this.profileCollection = this.afs.collection('Users', ref => ref.where("phoneNumber", "==", oldUser.phoneNumber));
      this.profileCollection.doc(oldUser.phoneNumber).delete().then(() => {
        console.log("Document successfully deleted!");
      }).catch((error) => {
        console.error("Error removing document: ", error);
      });
    }

    this.profileCollection = this.afs.collection('Users', ref => ref.where("phoneNumber", "==", updatedUser.phoneNumber));
    this.profileCollection.doc(updatedUser.phoneNumber).set(Object.assign({}, updatedUser));

  }

  getUsers() {
    this.profileCollection = this.afs.collection('Users');
    return this.profileCollection.get();
  }

  getSchools() {
    this.profileCollection = this.afs.collection('Schools');
    return this.profileCollection.get();
  }

  updaeProfile(user) {
    this.afs.collection("Schools").doc(user.phoneNumber).set(user);
  }

  public addschool(SchoolName, UDISENo, City, Address, Email, PrincipalName, Department) {
    const id = SchoolName;
    console.log(SchoolName);
    this.afs.collection("Schools").doc(id).set({
      SchoolName: SchoolName,
      UDISENo: UDISENo,
      Address: Address,
      City: City,
      Email: Email,
      PrincipalName: PrincipalName,
      Department: Department
    })
      .then(() => {
        console.log("School added successfully");
      })
      .catch((error) => {
        console.error("Error adding school: ", error);
      });
  }

  public getStudentsFilter(classId: string, schoolId: string) {
    this.studentsCollectionFilter = this.afs.collection('Schools/' + schoolId + '/Classes/' + classId + '/Students');
    this.studentsFilter = this.studentsCollectionFilter.snapshotChanges().pipe(map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as Student;
        data.id = a.payload.doc.id;
        return data;
      });
    })
    );
    return this.studentsFilter;
  }

  public getQuestionsFilter() {
    this.questionCollection = this.afs.collection('traits');
    this.questions = this.questionCollection.snapshotChanges().pipe(map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return data;
      });
    }));
    return this.questions;
  }

  public saveAssessment(assessment) {
    this.afs.collection("Assessments").doc(assessment.TimeStamp).set(assessment).then(async msg => {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Alert',
        message: 'Assessment Saved.',
        buttons: ['OK']
      });
      await alert.present();

    }).catch(async error => {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Alert',
        message: error,
        buttons: ['OK']
      });
      await alert.present();
    });
  }

  public getAssessmentsFilter(schoolId, classId, teacherId) {
    this.assessmentsCollection = this.afs.collection('Assessments', ref => {
      return ref
        .where("School", "==", schoolId)
        .where("Class", "==", classId)
        .where("Teacher", "==", teacherId)
    });
    this.assessments = this.assessmentsCollection.snapshotChanges().pipe(map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data();
        data.time = new Date(data.TimeStamp * 1);
        data.time = data.time.toUTCString();
        data.id = a.payload.doc.id;
        return data;
      });
    }));
    return this.assessments;
  }

}
