import { AngularFireStorage } from '@angular/fire/storage';
import { ActivatedRoute, Router } from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {AngularFirestore, QuerySnapshot} from '@angular/fire/firestore';
import {ClassModel} from './class-model';


@Component({
  selector: 'app-classes',
  templateUrl: './classes.page.html',
  styleUrls: ['./classes.page.scss'],
})
export class ClassesPage implements OnInit {
  schools:any = [];
  schoolName:string;
  classes:any = [];
  showClassOptions:boolean = false;
  emptyClassErr:boolean = false;
  hide:boolean = false;
  value:any;
  param:boolean = false;
  className: string;
  studentCount: number;

  constructor(private firebase: AngularFirestore,private route: ActivatedRoute,private router:Router) {
    this.route.params.subscribe(params => {
      console.log(params);
      if (params) {
        this.getSelected(params.schoolName);
        this.value = params.schoolName;
      }
    });

  }
   
  ngOnInit() {
    this.route.params.subscribe( params => {
      console.log(params)
      if(params.schoolName){
        console.log("Entered here")
        this.param = true;
        this.getSelected(params.schoolName); 
        this.value=params.schoolName;
      }else{
        this.param = false;
      }

      
    });
    this.schoolsList()
  }

          

  schoolsList() {
    const schools = this.firebase.collection('Schools').get().subscribe((doc) => {
      doc.forEach(e => {
        this.schools.push(e.id.toString());
      });
      console.log(this.schools);
    });

  }

  getSelected(value: string) {
    if (value) {
      this.schoolName = value;
      this.showClassOptions = true;
      this.classes = [];
      this.emptyClassErr = false;
      console.log(this.schoolName);
      if (this.schoolName != null) {
        this.viewClasses();
      }
    } else {
      this.showClassOptions = false;
    }

  }

  viewClasses() {
    this.classes = [];
    this.emptyClassErr = false;
    const path = 'Schools/' + this.schoolName + '/Classes';
    const classesCollection = this.firebase.collection(path);
    classesCollection.get().subscribe((doc: QuerySnapshot<ClassModel>) => {
      doc.forEach(e => {
          this.classes.push({
            name: e.id,
            stuCount: e.data().StuCount
          });
          console.log(e.id, '', e.data().StuCount);
        }
      );
      console.log('classes: ', this.classes);
      if (this.classes.length === 0) {
        console.log('empty');
        this.emptyClassErr = true;
      }
    });
  }


  async addClass(name: string, count: number) {
    if (name && count) {
      console.log('Path is ', this.schoolName);
      const path = 'Schools/' + this.schoolName + '/Classes';
      const schoolCollection = this.firebase.collection(path);
      const newClass = new ClassModel();
      newClass.StuCount = count;
      schoolCollection.doc(name).set(Object.assign({}, newClass));
      this.viewClasses();
    } else {
      this.hide = true;
    }
  }

  viewStudent(className: string){
    console.log(this.schoolName)
    this.router.navigate(['addstudents/'+this.schoolName+'/'+className]);
  }


}
