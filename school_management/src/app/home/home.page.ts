import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileModel } from '../profile/profile.model';
import { FirebaseAuthService } from '../services/firebase-auth.service';
import { DbService } from './../services/db.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  userProfile: ProfileModel;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: FirebaseAuthService,
    private dbService: DbService) {

  }

  ngOnInit() {

    this.userProfile = JSON.parse(localStorage.getItem('current_user'));
    console.log(this.userProfile);
  }

  showUserManagement() {
    return this.userProfile?.role.includes('admin') || this.userProfile?.role.includes('member');
  }

  showAddSchool() {
    return this.showUserManagement();
  }

  showAddClass() {
    return this.showUserManagement() || this.userProfile?.role.includes('principal');
  }

  showAddStudents() {
    return true;
  }

  showAssessments() {
    return this.userProfile?.role.includes('teacher');
  }
}
