export class UserModel {
  email: string;
  name: string;
  role: string;
  phoneNumber: string;
  school: string;
  dateOfJoining: string;
  gender: string;
  aadharId: string;
  // constructor(){
  //   this.id=null;
  //   this.image=null;
  //   this.name=null;
  //   this.role=null;
  //   this.email=null;
  //   this.provider=null;
  //   this.phoneNumber=null;
  // }

  public getData(): object {
    const result = {};
    Object.keys(this).map((key) => (result[key] = this[key]));
    return result;
  }
  public constructor(init?: Partial<UserModel>) {
    Object.assign(this, init);
}
}

