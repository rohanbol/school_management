import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import {HomePage} from '../../home/home.page';
import { RouterModule, Routes } from '@angular/router';

import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { UserManagementPage } from '../user-management.page';
import { AddUserPage } from './add-user.page';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['sign-in']);

const routes: Routes = [
  {
      path: '',
      component: AddUserPage,
      // children: [
      //   {
      //     path: 'addUser',
      //     children: [
      //       {
      //         path: '',
      //         loadChildren:() => import('./add-user.page').then(m => m.AddUserPage)
      //       }
      //     ]
      //   },
      //   {
      //     path: 'listUsers',
      //     children: [
      //       {
      //         path: '',
      //         loadChildren:() => import('../list-users/list-users.page').then(m => m.ListUsersPage)
      //       }
      //     ]
      //   },
      //   {
      //     path: '',
      //     redirectTo: 'user-management/addUser',
      //     pathMatch: 'full'
      //   }
      // ],
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  }
];
  
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [AddUserPage],
})
export class AddUserPageModule {}
