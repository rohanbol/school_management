import { DbService } from '../../services/db.service';
import { Component, OnInit,ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FirebaseAuthService } from '../../services/firebase-auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserModel } from '.././user.model';
import { CSVRecord } from '../CSVModel';  

@Component({
  selector: 'app-profile',
  templateUrl: './add-user.page.html',
  styleUrls: ['./add-user.page.scss'],
})
export class AddUserPage implements OnInit {
  public records: any[] = [];  
  @ViewChild('csvReader') csvReader: any;
  bulkUpload: boolean;
  addUserForm: FormGroup;
  user = new UserModel();
  schools:any = [];
  usersList = Array();
  validation_messages = {
    'email': [
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'phoneNumber': [
      { type: 'required', message: 'Phone Number is required.' },
      { type: 'pattern', message: 'Phone Number must be at 10 digits.' }
    ],
    'name': [
      { type: 'required', message: 'Name is required.' },
    ],
    'role': [
      { type: 'required', message: 'Role is required.' },
    ],
    'school': [
      { type: 'required', message: 'School is required.' },
    ],
    'aadharId': [
      { type: 'pattern', message: 'Incorrect Aadhar Number' },
      { type: 'required', message: 'Aadhar ID is required.' }
    ],
    'gender': [
      { type: 'required', message: 'Gender is required.' },
    ],
    'dateOfJoining': [
      { type: 'required', message: 'Date of Joining is required.' },
    ],
  };
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: FirebaseAuthService,
    private dbService: DbService,
    private firebase: AngularFirestore
  ) { 
    this.bulkUpload=false;
    this.addUserForm = new FormGroup({
      'email': new FormControl('', Validators.compose([
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      'phoneNumber': new FormControl('', Validators.compose([
        Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$"),
        Validators.required
      ])),
      'name': new FormControl('',Validators.compose([
        Validators.required
      ])),
      'role': new FormControl('',Validators.compose([
        Validators.required
      ])),
      'school': new FormControl('',Validators.compose([
        Validators.required
      ])),
      'aadharId': new FormControl('',Validators.compose([
        Validators.pattern("^[2-9]{1}[0-9]{3}[0-9]{4}[0-9]{4}$"),
        Validators.required
      ])),
      'gender': new FormControl('',Validators.compose([
        Validators.required
      ])),
      'dateOfJoining': new FormControl('',Validators.compose([
        Validators.required
      ]))
    });
  }

  ngOnInit() {
    var schools = this.firebase.collection("Schools").get().subscribe((doc) => {
      doc.forEach(e => {
        this.schools.push(e.id.toString());
      });
      console.log(this.schools);
});

  }
  addUserToDB(input){
    if(!this.bulkUpload){
      input=input.value;
    }
    this.user=  Object.assign(this.user, input);
    this.user.dateOfJoining=this.user.dateOfJoining.split('T')[0];
    this.dbService.addUser(this.user).subscribe((items)=> {
        window.location.href="/user-management/listUsers";
    });
  }
  
  onSubmit(){
    
    if(this.bulkUpload == false){
        this.addUserToDB(this.addUserForm);
    }
    else
      {
      for (var rec of this.records) {
      this.addUserToDB(rec);
      //this.dbService.addschool(rec.SchoolName,rec.UDISENo,rec.City,rec.Address, rec.Email,rec.PrincipalName,rec.Department);
      }
    }
  }

  uploadListener($event: any): void {  
  
    let text = [];  
    let files = $event.srcElement.files;  
    this.bulkUpload = true;
    if (this.isValidCSVFile(files[0])) {   
      let input = $event.target;  
      let reader = new FileReader();  
      reader.readAsText(input.files[0]);  
  
      reader.onload = () => {  
        let csvData = reader.result;  
        let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);  
  
        let headersRow = this.getHeaderArray(csvRecordsArray);  
  
        this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length); 
         console.log(this.records);
      };  
  
      reader.onerror = function () {  
        console.log('error is occured while reading file!');  
      };  
  
    } else {  
      alert("Please import valid .csv file.");  
      this.fileReset();  
    }
  }
  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {  
    let csvArr = [];  
  
    for (let i = 1; i < csvRecordsArray.length; i++) {  
      let currentRecord = (<string>csvRecordsArray[i]).split(',');  
      if (currentRecord.length == headerLength) {  
        let csvRecord: CSVRecord = new CSVRecord();  
        csvRecord.name = currentRecord[0].trim();  
        csvRecord.gender = currentRecord[1].trim();  
        csvRecord.school = currentRecord[2].trim();  
        csvRecord.aadharID = currentRecord[3].trim();  
        csvRecord.dateOfJoining = currentRecord[4].trim();  
        csvRecord.phoneNumber = currentRecord[5].trim();  
        csvRecord.email = currentRecord[6].trim();
        csvRecord.role = currentRecord[7].trim();
        csvArr.push(csvRecord);  
      }  
    }  
    return csvArr;  
  }  
  
  isValidCSVFile(file: any) {  
    return file.name.endsWith(".csv");  
  }  
  
  getHeaderArray(csvRecordsArr: any) {  
    let headers = (<string>csvRecordsArr[0]).split(',');  
    let headerArray = [];  
    for (let j = 0; j < headers.length; j++) {  
      headerArray.push(headers[j]);  
    }  
    return headerArray;  
  }  
  
  fileReset() {  
    this.csvReader.nativeElement.value = "";  
    this.records = [];  
  }

}
 


