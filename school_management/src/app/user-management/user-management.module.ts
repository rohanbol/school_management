import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import {HomePage} from '../home/home.page';
import { RouterModule, Routes } from '@angular/router';

import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { UserManagementPage } from './user-management.page';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['sign-in']);

const routes: Routes = [
  {
      path: '',
      component: UserManagementPage,
      children: [
        {
          path: 'addUser',
          children: [
            {
              path: '',
              loadChildren:() => import('../user-management/add-user/add-user.module').then(m => m.AddUserPageModule)
            }
          ]
        },
        {
          path: 'listUsers',
          children: [
            {
              path: '',
              loadChildren:() => import('../user-management/list-users/list-users.module').then(m => m.ListUsersPageModule)
            }
          ]
        },
        {
          path: '',
          redirectTo: 'addUser',
          pathMatch: 'full'
        }
      ],
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin },
  }
];
  
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  declarations: [UserManagementPage],
})
export class UserManagementModule {}
