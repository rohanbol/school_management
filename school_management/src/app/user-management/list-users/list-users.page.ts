import { DbService } from '../../services/db.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FirebaseAuthService } from '../../services/firebase-auth.service';
import { UserModel } from '../user.model';

@Component({
  selector: 'app-profile',
  templateUrl: './list-users.page.html',
  styleUrls: ['./list-users.page.scss'],
})
export class ListUsersPage implements OnInit {
  
  usersList = Array();
  rolesMap : Map<String,String>;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: FirebaseAuthService,
    private dbService: DbService
  ) { 
      this.rolesMap = new Map<String,String>();
      this.rolesMap.set("teacher","Teacher");
      this.rolesMap.set("member","SEF Member");
      this.rolesMap.set("principal","Principal");
      this.rolesMap.set("admin","SEF Admin");
      this.rolesMap.set("Teacher","Teacher");
      this.rolesMap.set("Member","SEF Member");
      this.rolesMap.set("Principal","Principal");
      this.rolesMap.set("SEF Admin","SEF Admin");
  }
  ngOnInit() {
     this.dbService.getUsers().subscribe((users)=>{
       this.usersList= new Array();
       users.docs.forEach(e => {
        let u = new UserModel();
         this.usersList.push(Object.assign(u,e.data()))
       });
       console.log(this.usersList);

     });
    
  }

}
 


