import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { combineLatest } from 'rxjs';
import { ProfileModel } from '../profile/profile.model';
import { DbService } from '../services/db.service';

@Component({
  selector: 'app-assessments',
  templateUrl: './assessments.page.html',
  styleUrls: ['./assessments.page.scss'],
})
export class AssessmentsPage implements OnInit {

  userProfile: ProfileModel;
  schoolId:string;
  classId:string;
  Classes = [];
  selectedPath = '';


  constructor(private dbService: DbService, private router:Router) {

    this.router.events.subscribe((event: RouterEvent) => {
      if(event && event.url){
        this.selectedPath = event.url;
      }
    });

   }

  ngOnInit() {

    this.userProfile=JSON.parse(localStorage.getItem('current_user'));
    console.log(this.userProfile);
    this.schoolId=this.userProfile.school;
    this.Classes = Object.keys(this.userProfile.classes);
    console.log(this.schoolId);
    console.log(this.Classes);
  }

  


}
