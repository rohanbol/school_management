import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssessmentsPage } from './assessments.page';

const routes: Routes = [
  {
  path: '',
  component: AssessmentsPage,
  children: [
    {
      path: 'create',
      children: [
        {
          path: ':schoolId/:className',
          loadChildren:() => import('./create/create.module').then( m => m.CreatePageModule)
        }
      ]
    },
    {
      path: 'modify',
      children: [
        {
          path: ':schoolId/:className',
          loadChildren:() => import('./modify/modify.module').then( m => m.ModifyPageModule)
        }
      ]
    },
    {
      path: '',
      redirectTo: '',
      pathMatch: 'full'
    }
  ],

  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssessmentsPageRoutingModule {}
