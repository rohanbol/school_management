import { ProfileModel } from './../../profile/profile.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DbService } from 'src/app/services/db.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-modify',
  templateUrl: './modify.page.html',
  styleUrls: ['./modify.page.scss'],
})
export class ModifyPage implements OnInit {

  schoolId: string;
  classId: string;
  currentAssessment: any;
  studentId: string;
  students: any;
  questions: any;
  userProfile: ProfileModel;
  assessments:any;
  flag=0;

  constructor(private dbService: DbService,private route: ActivatedRoute) {
    this.userProfile = JSON.parse(localStorage.getItem('current_user'));
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      //console.log(params.schoolId, params.className);
      this.schoolId = params.schoolId;
      this.classId = params.className;

      let assessmentsObservable = this.dbService.getAssessmentsFilter(this.schoolId,this.classId,this.userProfile.phoneNumber);
      let questionsObservable = this.dbService.getQuestionsFilter();

      assessmentsObservable.subscribe(
        (assessments) => {
          this.assessments=assessments
          //console.log(this.assessments);
        })

        combineLatest([assessmentsObservable, questionsObservable]).subscribe(
          (results) => {
            this.assessments== results[0]
            //console.log(this.assessments);
            this.questions = results[1];
            //console.log(this.questions);
          })
      

    })

  }

  selectItem(assessment){
    this.currentAssessment=assessment;
    this.students=Object.keys(this.currentAssessment.Grades);
    //console.log(this.students);
    this.flag=1;
  }

  openItem(selectStudent){
    this.studentId=selectStudent
    this.flag=2;

  }

  back(id){
    this.flag=id;
  }

  saveAssessment(){
    console.log(this.currentAssessment);
    this.dbService.saveAssessment(this.currentAssessment)
    this.flag=0;
    
  }



}
