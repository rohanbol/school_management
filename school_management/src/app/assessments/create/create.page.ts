import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';
import { ProfileModel } from 'src/app/profile/profile.model';
import { DbService } from 'src/app/services/db.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {

  userProfile: ProfileModel;
  flag: number = 0;
  schoolId: string;
  classId: string;
  assessment: any = {};
  students = [];
  questions: any;
  studentId: string;
  studentName: string;
  assessmentName: string="";

  constructor(private dbService: DbService, private route: ActivatedRoute) {
    this.userProfile = JSON.parse(localStorage.getItem('current_user'));
  }

  ngOnInit() {

    this.route.params.subscribe(params => {

      //console.log(params.schoolId, params.className);
      this.schoolId = params.schoolId;
      this.classId = params.className;

      let studentsObservable = this.dbService.getStudentsFilter(this.classId, this.schoolId);
      let questionsObservable = this.dbService.getQuestionsFilter();

      combineLatest([studentsObservable, questionsObservable]).pipe(take(1)).subscribe(
        (results) => {
          this.students = results[0];
          //console.log(this.students);
          this.questions = results[1];
          //console.log(this.questions);

          this.assessment["Class"] = this.classId;
          this.assessment["School"] = this.schoolId;
          this.assessment["Subject"] = "Behavioral";
          this.assessment["Teacher"] = this.userProfile.phoneNumber;
          this.assessment["TimeStamp"] = Date.now().toString();
          this.assessment["Grades"] = {};
          this.students.forEach(student => {
            let initialGrades = new Array(this.questions.length).fill(false);
            this.assessment.Grades[student.rollNo + "-" + student.name] = initialGrades;
          })
          //console.log(this.assessment);
        })

    }

    )
  }

  openItem(itemId,itemName) {
    this.studentId=itemId
    this.studentName=itemName
    //console.log(this.studentId);
    this.flag=1;
    //console.log(this.assessment.Grades[this.studentId][0]);
  }

  back(){
    this.flag=0;
    //console.log(this.assessment.Grades);
  }

  saveAssessment(){
    this.assessment["Name"] = this.assessmentName;
    console.log(this.assessment.Grades);
    this.dbService.saveAssessment(this.assessment);
  }

}
