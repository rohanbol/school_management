
import { Component, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FirebaseAuthService } from '../services/firebase-auth.service';
import { Subscription } from 'rxjs';
import { WindowService } from '../services/window.service';
import * as firebase from 'firebase';
import { DbService } from '../services/db.service';
import { take } from 'rxjs/operators';



@Component({
  selector: 'app-sign-in',
  templateUrl: 'sign-in.page.html',
  styleUrls: ['sign-in.page.scss'],
})
export class SignInPage {
  emailSignIn: boolean;
  signInForm: FormGroup;
  submitError: string;
  authRedirectResult: Subscription;
  line: any;
  prefix = '+91';
  verifCode: any;
  windowRef: any;
  enableButton = false;


  validationMessages = {
    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    password: [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' }
    ]
  };


  constructor(
    public windowService: WindowService,
    public angularFire: AngularFireAuth,
    public router: Router,
    private ngZone: NgZone,
    private authService: FirebaseAuthService,
    private dbService: DbService
  ) {
    this.emailSignIn = false;

    this.signInForm = new FormGroup({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ]))
    });

    // if user has already logged in redirects back to the home
    this.authService.getUserLoggedInStatus().subscribe(user => {
      if (user)
        this.router.navigate(['home']);
    });
    // Get firebase authentication redirect result invoken when using signInWithRedirect()
    // signInWithRedirect() is only used when client is in web but not desktop
    this.authRedirectResult = this.authService.getRedirectResult()
      .subscribe(result => {
        console.log(firebase.auth().currentUser.email);
        if (result.user) {
          this.dbService.getProfileByEmail(firebase.auth().currentUser.email).pipe(take(1)).subscribe((loggedInUser) => {
            if (loggedInUser.length > 0) {
              localStorage.setItem('current_user', JSON.stringify(loggedInUser[0]));
              this.redirectLoggedUserToHomePage();
            } else {
              this.submitError = "You are not authorized to login. Please ask SEF Admin to add your phone number";
            }
          });
        } else if (result.error) {
          this.submitError = result.error;
        }
      });
  }


  // Once the auth provider finished the authentication flow, and the auth redirect completes,
  // redirect the user to the profile page
  redirectLoggedUserToHomePage() {
    // As we are calling the Angular router navigation inside a subscribe method, the navigation will be triggered outside Angular zone.
    // That's why we need to wrap the router navigation call inside an ngZone wrapper
    this.ngZone.run(() => {
      this.router.navigate(['home']);
    });
  }

  ionViewWillEnter() {
    this.windowRef = this.windowService.windowRef;
    let self = this;
    // this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
    //   'size': 'invisible',
    //   'callback': function(response) {
    //     console.log(response);
    //     self.sendLoginCode();
    //   }
    // });
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    this.windowRef.recaptchaVerifier.render();
  }

  sendLoginCode() {//Make sure phone number in e164 format
    this.submitError = "";
    this.dbService.getProfile(this.line.toString()).subscribe((user) => {
      if (user.data()) {
        localStorage.setItem('current_user', JSON.stringify(user.data()));
        this.enableButton = true;
        console.log("Sending Code");
        const num = this.prefix.toString() + this.line.toString();
        const appVerifier = this.windowRef.recaptchaVerifier;
        firebase.auth().signInWithPhoneNumber(num, appVerifier)
          .then(result => {
            this.windowRef.confirmationResult = result;
          })
          .catch(err => console.log('err1', err))

      } else {
        //TODO - change the error
        this.submitError = "You are not authorized to login. Please ask SEF Admin to add your phone number";
      }
    })

  }
  submitVerif() {
    this.windowRef.confirmationResult.confirm(this.verifCode)
      .then(async user => {
        console.log(user);
        this.redirectLoggedUserToHomePage();
        this.line = null;
        this.enableButton = false;
      })
      .catch(err => {
        console.log('err2', err);
      });
  }

  phoneSignInButton() {
    //this.router.navigate(['sign-in']);
    location.reload();
    // this.emailSignIn=false;
  }

  emailSignInButton() {
    this.emailSignIn = true;
  }

  signInWithEmail() {
    this.authService.signInWithEmail(this.signInForm.value['email'], this.signInForm.value['password'])
      .then(user => {
        console.log(this.signInForm.value['email']);
        this.dbService.getProfileByEmail(this.signInForm.value['email']).pipe(take(1)).subscribe((loggedInUser) => {
          if (loggedInUser.length > 0) {
            localStorage.setItem('current_user', JSON.stringify(loggedInUser[0]));
            this.redirectLoggedUserToHomePage();
          } else {
            this.submitError = "You are not authorized to login. Please ask SEF Admin to add your phone number";
          }
        });
      })
      .catch(error => {
        this.submitError = error.message;
      });
  }

  facebookSignIn() {
    this.authService.signInWithFacebook()
      .then((result: any) => {
        if (result.additionalUserInfo) {
          this.authService.setProviderAdditionalInfo(result.additionalUserInfo.profile);
        }
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        // const token = result.credential.accessToken;
        // The signed-in user info is in result.user;
        this.redirectLoggedUserToHomePage();
      }).catch((error) => {
        // Handle Errors here.
        console.log(error);
      });
  }

  googleSignIn() {
    console.log("in google signin ")
    this.authService.signInWithGoogle()
      .then((result: any) => {
        if (result.additionalUserInfo) {
          this.authService.setProviderAdditionalInfo(result.additionalUserInfo.profile);
        }

        this.dbService.getProfileByEmail(result.user.email).pipe(take(1)).subscribe((user) => {
          console.log(user);
          if (user.length >= 1) {
            localStorage.setItem('current_user', JSON.stringify(user[0]));
            //this.redirectLoggedUserToHomePage();
          } else {
            //TODO - change the error
            this.submitError = "You are not authorized to sign in. Please ask SEF Admin to add your profile";
          }

        });


        // This gives you a Google Access Token. You can use it to access the Google API.
        // const token = result.credential.accessToken;
        // The signed-in user info is in result.user;
        //this.redirectLoggedUserToHomePage();
      }).catch((error) => {
        // Handle Errors here.
        console.log(error);
      });
  }

  twitterSignIn() {
    this.authService.signInWithTwitter()
      .then((result: any) => {
        if (result.additionalUserInfo) {
          this.authService.setProviderAdditionalInfo(result.additionalUserInfo.profile);
        }
        // This gives you a Twitter Access Token. You can use it to access the Twitter API.
        // const token = result.credential.accessToken;
        // The signed-in user info is in result.user;
        this.redirectLoggedUserToHomePage();
      }).catch((error) => {
        // Handle Errors here.
        console.log(error);
      });
  }
}
