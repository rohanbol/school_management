export interface Student {
  id?: string;
  rollNo: string;
  name: string;
  guardianName: string;
  aadharId: string;
  dateofBirth: string;
  gender: string;
  classId: string;
  schoolId: string;
}
