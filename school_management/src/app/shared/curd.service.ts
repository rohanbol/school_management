import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Student} from './student';

@Injectable({
  providedIn: 'root'
})
export class CurdService {
  studentsCollectionFilter: AngularFirestoreCollection<Student>;
  students: Observable<Student[]>;
  studentsFilter: Observable<Student[]>;
  studentDoc: AngularFirestoreDocument<Student>;

  constructor(public firestore: AngularFirestore) {

  }

  getstudents() {
    return this.studentsFilter;
  }

  getStudentsFilter(classId: string, schoolId: string) {
    this.studentsCollectionFilter = this.firestore.collection('Schools/' + schoolId + '/Classes/' + classId + '/Students');
    this.studentsFilter = this.studentsCollectionFilter.snapshotChanges().pipe(map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as Student;
          data.id = a.payload.doc.id;
          return data;
        });
      })
    );
    return this.studentsFilter;
  }
}
