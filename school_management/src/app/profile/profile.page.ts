import { DbService } from './../services/db.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileModel } from './profile.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { take } from 'rxjs/operators';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';




@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user: ProfileModel;
  userProfile: ProfileModel;
  updatedUser=new ProfileModel;
  flag = false;
  schools:any = [];
  addUserForm: FormGroup;
  userClasses:any=[];
  availableClasses:any=[];
  availableSubjects:any=["Hindi","Telugu","English","Maths","Science","Social","Computers"]

  validation_messages = {
    'email': [
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'phoneNumber': [
      { type: 'required', message: 'Phone Number is required.' },
      { type: 'pattern', message: 'Phone Number must be at 10 digits.' }
    ],
    'name': [
      { type: 'required', message: 'Name is required.' },
    ],
    'role': [
      { type: 'required', message: 'Role is required.' },
    ],
    'school': [
      { type: 'required', message: 'School is required.' },
    ],
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dbService: DbService,
    private firebase: AngularFirestore,
    private fb:FormBuilder
  ) { 

    


  }

  ngOnInit() {


    this.route.data.subscribe((result) => {
      this.user = result['data'];

      this.userProfile=JSON.parse(localStorage.getItem('current_user'));
      console.log(this.userProfile);
      this.initialiseForm();

      // if(this.user.phoneNumber){
      //   this.dbService.getProfile(this.user.phoneNumber.replace("+91","")).subscribe((_) =>{
      //     console.log(_.data())
      //     this.userProfile=_.data();
      //     this.initialiseForm();
      //   });
      
      // }

      // else if(this.user.email){
      //   this.dbService.getProfileByEmail(this.user.email).pipe(take(1)).subscribe((_)=>{
      //     console.log(_[0])
      //     this.userProfile=_[0];
      //     this.initialiseForm();
      //   });
      // }

      // else{
      //   console.log("Unknown User!");
      // }

    }, (err) => {})

    var schools = this.firebase.collection("Schools").get().subscribe((doc) => {
      doc.forEach(e => {
        this.schools.push(e.id.toString());
      });
    });

    

    

    // this.addUserForm = this.fb.group({

    //   name: '',
    //   phoneNumber: '',
    //   email:'',
    //   school:'',

    //   quantities: this.fb.array([]) ,

    // });

    //console.log("Inside profile page with user profile"+ this.userProfile[0]);
  }

  initialiseForm(){

    this.addUserForm = new FormGroup({
      'email': new FormControl(this.userProfile.email, Validators.compose([
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      'phoneNumber': new FormControl(this.userProfile.phoneNumber, Validators.compose([
        Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$"),
        Validators.required
      ])),
      'name': new FormControl(this.userProfile.name,Validators.compose([
        Validators.required
      ])),
      'role': new FormControl(this.userProfile.role,Validators.compose([
        Validators.required
      ])),
      'school': new FormControl(this.userProfile.school,Validators.compose([
        Validators.required
      ])),
      'classes': this.fb.array(this.userClasses) ,

    });
    console.log(this.userProfile.classes);

  //   let path = "Users/"+this.userProfile.phoneNumber+"/Classes";
  //   var classes_collection = this.firebase.collection(path);
  //   classes_collection.get().subscribe((doc) => {
  //       doc.forEach(e => {
  //         this.addExistingClass(e.id,e.data()['subject']);
  //       });       
  // });

    var availableClasses = this.firebase.collection("Schools/"+this.userProfile.school+"/Classes").get().subscribe((doc) => {
      doc.forEach(e => {
        this.availableClasses.push(e.id.toString());
      });
    });

    console.log(this.availableClasses);

    for (let className in this.userProfile.classes){
      this.userProfile.classes[className].forEach(subjectName =>{
        this.addExistingClass(className,subjectName);
      });
    }


  }

  classes() : FormArray {
    return this.addUserForm.get("classes") as FormArray
  }

   

  newClass(): FormGroup {
    return this.fb.group({
      class: '',
      subject: '',
    })
  }

  insertExistingItem(name:string, subject:string){
    return this.fb.group({
      class: name,
      subject: subject,
    });
  }
  

  addExistingClass( name:string, subject:string ) {
    this.userClasses=this.addUserForm.get("classes") as FormArray;
    this.userClasses.push(this.insertExistingItem(name,subject));
  }

   

  addClass() {
    this.classes().push(this.newClass());
  }

  removeClass(i:number) {
    this.classes().removeAt(i);
  }



  edit(){
    this.flag=true;
  }

  updateUserInDB(){

    this.updatedUser =  Object.assign(this.updatedUser, this.addUserForm.value);

    this.updatedUser.classes = {};

    this.addUserForm.value.classes.forEach(classItem => {
       if (this.updatedUser.classes.hasOwnProperty(classItem.class)){
        this.updatedUser.classes[classItem.class].push(classItem.subject);
       }
       else{
        this.updatedUser.classes[classItem.class] = [classItem.subject];
       }
    });

    this.dbService.updateUser(this.updatedUser,this.userProfile);
    localStorage.setItem("current_user",JSON.stringify(this.updatedUser));
    this.userProfile=Object.assign({},this.updatedUser);
    this.flag=false;
  }

}
 


