import { map } from 'rxjs/operators';
export class ProfileModel {
  id: string;
  image: string;
  name: string;
  role: string;
  email: string;
  provider: string;
  phoneNumber: string;
  school: string;
  classes: any;


  // constructor(){
  //   this.id=null;
  //   this.image=null;
  //   this.name=null;
  //   this.role=null;
  //   this.email=null;
  //   this.provider=null;
  //   this.phoneNumber=null;
  // }

  public getData(): object {
    const result = {};
    Object.keys(this).map((key) => (result[key] = this[key]));
    return result;
  }
}

