import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { FirebaseAuthService } from './services/firebase-auth.service';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  route: string;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: FirebaseAuthService,
    private router: Router,
    location: Location
    
  ) {
    this.initializeApp();
    router.events.subscribe((val) => {
      if(location.path() != ''){
        this.route = location.path();
      } else {
        this.route = 'Home'
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  signOut() {
    console.log(this.authService);
    this.authService.signOut().subscribe(() => {
      localStorage.removeItem("current_user");
      // Sign-out successful.
      localStorage.removeItem("current_user");
      this.router.navigate(['sign-in']);
    }, (error) => {
      console.log('signout error', error);
    });
  }
  

}
