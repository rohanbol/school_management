import { Component, OnInit,ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FirebaseAuthService } from '../services/firebase-auth.service';
import { DbService } from './../services/db.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { CSVRecord } from './CSVModel';  
import { SchoolModel } from './school.model';

@Component({
  selector: 'add-school',
  templateUrl: './add-school.page.html',
  styleUrls: ['./add-school.page.scss'],
})
export class AddSchoolPage implements OnInit {
  public records: any[] = [];  
  @ViewChild('csvReader') csvReader: any;
  addschool: boolean;
  schoolList= Array();
  AddSchoolForm: FormGroup;
  constructor(private dbService: DbService,private router: Router) { this.addschool = false;}
    validationMessages = {
    SchoolName: [
      { type: 'required', message: 'School is required.' },
      { type: 'pattern', message: 'Enter a valid school' }
    ],
    UDISENo: [
      { type: 'required', message: 'UDISENo is required.' },
      { type: 'pattern', message: 'Enter valid number' }
    ],
    City: [
      { type: 'required', message: 'City is required.' },
      { type: 'pattern', message: 'Enter a valid city' }
    ],
    Email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter valid email' }
    ],
    PrincipalName: [
      { type: 'required', message: 'Principal Name is required.' },
      { type: 'pattern', message: 'Enter a valid principal name' }
    ],
    Department: [
      { type: 'required', message: 'Department is required.' },
      { type: 'pattern', message: 'Enter valid department' }
    ]
  };

  ngOnInit() {
    this.initForm();
    this.initSchools();
  }

  initSchools(){
    this.dbService.getSchools().subscribe((users)=>{
      this.schoolList= new Array();
      users.docs.forEach(e => {
       let u = new SchoolModel();
        this.schoolList.push(Object.assign(u,e.data()))
      });
      console.log(this.schoolList);

    });
  }
  initForm(){
    this.AddSchoolForm = new FormGroup({
      SchoolName: new FormControl('',Validators.compose([
        Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')])),
      UDISENo: new FormControl('',Validators.compose([
        Validators.required, Validators.pattern('[0-9]+')])),
      City: new FormControl('',Validators.compose([
        Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')])),
      Address: new FormControl('',Validators.compose([
        Validators.required])),
      Email: new FormControl('',Validators.compose([
        Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])),
      PrincipalName: new FormControl('',Validators.compose([
        Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')])),
      Department: new FormControl('',Validators.compose([
        Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')])),
    });
    //this.onSubmit()
   //this.dbservice.addschool(this.itemForm.value);
}
onSubmit()
{
  if(this.addschool == false)
    this.dbService.addschool(this.AddSchoolForm.value['SchoolName'],this.AddSchoolForm.value['UDISENo'],this.AddSchoolForm.value['City'],this.AddSchoolForm.value['Address'], this.AddSchoolForm.value['Email'],this.AddSchoolForm.value['PrincipalName'],this.AddSchoolForm.value['Department']);
    else
    {
    for (var rec of this.records) {
    this.dbService.addschool(rec.SchoolName,rec.UDISENo,rec.City,rec.Address, rec.Email,rec.PrincipalName,rec.Department);
    }
  }
  this.initSchools();
}
viewClass(name){
  this.router.navigate(['classes/'+name]);

}

uploadListener($event: any): void {  
  
  let text = [];  
  let files = $event.srcElement.files;  
  this.addschool = true;
  if (this.isValidCSVFile(files[0])) {   
    let input = $event.target;  
    let reader = new FileReader();  
    reader.readAsText(input.files[0]);  

    reader.onload = () => {  
      let csvData = reader.result;  
      let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);  

      let headersRow = this.getHeaderArray(csvRecordsArray);  

      this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length); 
       
    };  

    reader.onerror = function () {  
      console.log('error is occured while reading file!');  
    };  

  } else {  
    alert("Please import valid .csv file.");  
    this.fileReset();  
  }
}
getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {  
  let csvArr = [];  

  for (let i = 1; i < csvRecordsArray.length; i++) {  
    let curruntRecord = (<string>csvRecordsArray[i]).split(',');  
    if (curruntRecord.length == headerLength) {  
      let csvRecord: CSVRecord = new CSVRecord();  
      csvRecord.SchoolName = curruntRecord[0].trim();  
      csvRecord.UDISENo = curruntRecord[1].trim();  
      csvRecord.City = curruntRecord[2].trim();  
      csvRecord.Address = curruntRecord[3].trim();  
      csvRecord.Email = curruntRecord[4].trim();  
      csvRecord.PrincipalName = curruntRecord[5].trim();  
      csvRecord.Department = curruntRecord[5].trim();  
      csvArr.push(csvRecord);  
    }  
  }  
  return csvArr;  
}  

isValidCSVFile(file: any) {  
  return file.name.endsWith(".csv");  
}  

getHeaderArray(csvRecordsArr: any) {  
  let headers = (<string>csvRecordsArr[0]).split(',');  
  let headerArray = [];  
  for (let j = 0; j < headers.length; j++) {  
    headerArray.push(headers[j]);  
  }  
  return headerArray;  
}  

fileReset() {  
  this.csvReader.nativeElement.value = "";  
  this.records = [];  
}
}