import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: 'sign-in',
    loadChildren: () => import('./sign-in/sign-in.module').then(m => m.SignInPageModule)
  },
  {
    path: 'sign-up',
    loadChildren: () => import('./sign-up/sign-up.module').then(m => m.SignUpPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then(m => m.ProfilePageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'user-management',
    loadChildren: () => import('./user-management/user-management.module').then(m => m.UserManagementModule)
  },
  {
    path: 'add-school',
    loadChildren: () => import('./add-school/add-school.module').then(m => m.AddSchoolPageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'addstudents/:schoolId/:className',
    loadChildren: () => import('./addstudents/addstudents.module').then(m => m.AddstudentsPageModule)
  },
  {
    path: 'addstudents',
    loadChildren: () => import('./addstudents/addstudents.module').then(m => m.AddstudentsPageModule)
  },
  // {
  //   path: 'add-students/:schoolId',
  //   loadChildren: () => import('./addstudents/addstudents.module').then(m => m.AddstudentsPageModule)
  // },
  {
    path: 'add-class',
    loadChildren: () => import('./classes/classes.module').then(m => m.ClassesPageModule)
  },
  {
    path: 'classes/:schoolName',
    loadChildren: () => import('./classes/classes.module').then(m => m.ClassesPageModule)
  },
  {
    path: 'assessments',
    loadChildren: () => import('./assessments/assessments.module').then( m => m.AssessmentsPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
