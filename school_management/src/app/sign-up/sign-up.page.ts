import { Component, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FirebaseAuthService } from '../services/firebase-auth.service';
import { Subscription } from 'rxjs';
import { DbService } from '../services/db.service';
import { take } from 'rxjs/operators';
import * as firebase from 'firebase';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage {
  signUpForm: FormGroup;
  submitError: string;
  authRedirectResult: Subscription;

  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' }
    ]
  };

  constructor(
    public angularFire: AngularFireAuth,
    public router: Router,
    private ngZone: NgZone,
    private authService: FirebaseAuthService,
    private dbService: DbService
  ) {


    this.signUpForm = new FormGroup({
      'email': new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      'password': new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ]))
    });
    // Get firebase authentication redirect result invoken when using signInWithRedirect()
    // signInWithRedirect() is only used when client is in web but not desktop
    this.authRedirectResult = this.authService.getRedirectResult()
      .subscribe(result => {
        if (result.user) {
          this.dbService.getProfileByEmail(firebase.auth().currentUser.email).pipe(take(1)).subscribe((loggedInUser) => {
            if (loggedInUser.length > 0) {
              localStorage.setItem('current_user', JSON.stringify(loggedInUser[0]));
              this.redirectLoggedUserToHomePage();
            } else {
              this.submitError = "You are not authorized to login. Please ask SEF Admin to add your phone number";
            }
          });
        } else if (result.error) {
          this.submitError = result.error;
        }
      });

    this.authService.getUserLoggedInStatus().subscribe(user => {
      if (user) this.router.navigate(['home']);
    });


  }

  // Once the auth provider finished the authentication flow, and the auth redirect completes,
  // redirect the user to the profile page
  redirectLoggedUserToHomePage() {
    // As we are calling the Angular router navigation inside a subscribe method, the navigation will be triggered outside Angular zone.
    // That's why we need to wrap the router navigation call inside an ngZone wrapper
    this.ngZone.run(() => {
      this.router.navigate(['home']);
    });
  }

  signUpWithEmail() {
    this.dbService.getProfileByEmail(this.signUpForm.value['email']).pipe(take(1)).subscribe((user) => {
      console.log(user);
      if (user.length >= 1) {
        this.authService.signUpWithEmail(this.signUpForm.value['email'], this.signUpForm.value['password'])
          .then(loggedInuser => {
            console.log(loggedInuser);
            // navigate to user profile
            localStorage.setItem('current_user', JSON.stringify(user[0]));
            this.redirectLoggedUserToHomePage();
          })
          .catch(error => {
            this.submitError = error.message;
          });
      } else {
        //TODO - change the error
        this.submitError = "You are not authorized to sign up. Please ask SEF Admin to add your profile";
      }
    });
  }

  facebookSignUp() {
    this.authService.signInWithFacebook()
      .then((result: any) => {
        if (result.additionalUserInfo) {
          this.authService.setProviderAdditionalInfo(result.additionalUserInfo.profile);
        }
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        // const token = result.credential.accessToken;
        // The signed-in user info is in result.user;
        this.redirectLoggedUserToHomePage();
      }).catch((error) => {
        // Handle Errors here.
        console.log(error);
      });
  }

  googleSignUp() {
    console.log("in google singup")
    this.authService.signInWithGoogle()
      .then((result: any) => {
        if (result.additionalUserInfo) {
          this.authService.setProviderAdditionalInfo(result.additionalUserInfo.profile);
        }

        this.dbService.getProfileByEmail(result.user.email).pipe(take(1)).subscribe((user) => {
          if (user.length >= 1) {
            localStorage.setItem('current_user', JSON.stringify(user[0]));
            this.redirectLoggedUserToHomePage();
          } else {
            //TODO - change the error
            this.submitError = "You are not authorized to sign up. Please ask SEF Admin to add your profile";
          }
        });
        // This gives you a Google Access Token. You can use it to access the Google API.
        // const token = result.credential.accessToken;
        // The signed-in user info is in result.user;
        //this.redirectLoggedUserToHomePage();
      }).catch((error) => {
        // Handle Errors here.
        console.log(error);
      });
  }

  twitterSignUp() {
    this.authService.signInWithTwitter()
      .then((result: any) => {
        if (result.additionalUserInfo) {
          this.authService.setProviderAdditionalInfo(result.additionalUserInfo.profile);
        }
        // This gives you a Twitter Access Token. You can use it to access the Twitter API.
        // const token = result.credential.accessToken;
        // The signed-in user info is in result.user;
        this.redirectLoggedUserToHomePage();
      }).catch((error) => {
        // Handle Errors here.
        console.log(error);
      });
  }
}
